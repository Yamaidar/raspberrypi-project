import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable, of, Subscription} from 'rxjs';
import * as moment from 'moment';
import {IMqttMessage, MqttService} from 'ngx-mqtt';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit, OnDestroy {

  constructor(private fb: FormBuilder, private _mqttService: MqttService) {
  }

  form: FormGroup;

  title = 'raspberry-front';
  authorizedIds: string[] = [];
  logs: any[] = [];

  loading = false;
  moment: any;
  client: any;

  TOPIC_ADD_ALLOWED = 'testtopic/acs/77/add';
  TOPIC_REMOVE_ALLOWED = 'testtopic/acs/77/remove';
  TOPIC_SET_ALLOWED = 'testtopic/acs/77/set';
  TOPIC_REQUEST_ALLOWED = 'testtopic/acs/77/request';

  TOPIC_CURRENT_ALLOWED = 'testtopic/acs/77/current_allowed';
  TOPIC_CHECKS_LOG = 'testtopic/acs/77/checks_log';

  private subscription: Subscription;
  public message: string;

  public unsafePublish(topic: string, message: string): void {
    this._mqttService.unsafePublish(topic, message, {qos: 1});
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      value: ['', [Validators.required]]
    });

    this.subscription = this._mqttService.observe(this.TOPIC_CHECKS_LOG).subscribe((message: IMqttMessage) => {
      let payload = JSON.parse(message.payload.toString());
      let date = moment().format('HH:mm:ss');
      payload.date = date;
      this.logs.push(payload);
    });

    this.subscription.add(this._mqttService.observe(this.TOPIC_CURRENT_ALLOWED).subscribe((message: IMqttMessage) => {
      let payload = JSON.parse(message.payload.toString());
      console.log('current allowed', payload);
      this.clearAllAuthorized(false);
      this.loading = false;
      this.authorizedIds = payload;
    }));
  }

  clearLogs() {
    this.logs = [];
  }

  removeAuthorized(value) {
    const index = this.authorizedIds.indexOf(value);
    if (index !== -1) {
      let payload = JSON.stringify({id: value});
      console.log(payload);
      this.unsafePublish(this.TOPIC_REMOVE_ALLOWED, payload);
      this.authorizedIds.splice(index, 1);
    }
  }

  addAuthorized(value) {
    const index = this.authorizedIds.indexOf(value);
    if (index === -1) {
      let payload = JSON.stringify({id: value});
      console.log(payload);
      this.unsafePublish(this.TOPIC_ADD_ALLOWED, payload);
      this.authorizedIds.push(value);
    }
  }

  addFromInput() {
    if (!this.form.invalid) {
      const value: string = this.form.value['value'];
      value
        .split(',')
        .map(x => x.trim())
        .forEach(x => this.addAuthorized(x));
    }
  }

  clearAllAuthorized(notify = true) {
    this.authorizedIds = [];
    if (notify)
      this.unsafePublish(this.TOPIC_SET_ALLOWED, '[]');
  }

  getFromClient() {
    this.loading = true;
    this.unsafePublish(this.TOPIC_REQUEST_ALLOWED, '');
  }
}
