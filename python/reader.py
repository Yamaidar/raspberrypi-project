import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522


class IdsReader:
    reader = SimpleMFRC522()

    def read_id(self):
        try:
            id, text = self.reader.read()
            print("readed id:" + str(id))
            return str(id)
        finally:
            print("finished")
