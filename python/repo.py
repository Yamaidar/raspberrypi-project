import json
import threading
from os import path

import paho.mqtt.client as mqtt

STORAGE = "./allowedIds.json"

BROKER_HOST = "broker.mqttdashboard.com"
BROKER_PORT = 1883

TOPIC_ADD_ALLOWED = "testtopic/acs/77/add"
TOPIC_REMOVE_ALLOWED = "testtopic/acs/77/remove"
TOPIC_SET_ALLOWED = "testtopic/acs/77/set"
TOPIC_REQUEST_ALLOWED = "testtopic/acs/77/request"

TOPIC_CURRENT_ALLOWED = "testtopic/acs/77/current_allowed"
TOPIC_CHECKS_LOG = "testtopic/acs/77/checks_log"


class IdsRepository:
    __allowed_ids = []
    __client = mqtt.Client()

    def check(self, id):
        allowed = id in self.__allowed_ids
        self.__log_check(id, allowed)
        return allowed

    def __load_from_file(self):
        with open(STORAGE) as json_file:
            self.__allowed_ids = json.load(json_file)

    def __save_to_file(self):
        with open(STORAGE, 'w') as json_file:
            json.dump(self.__allowed_ids, json_file)

    def __log_check(self, id, is_allowed):
        payload = {'id': id, 'is_allowed': is_allowed}
        self.__client.publish(TOPIC_CHECKS_LOG, payload=json.dumps(payload))

    def __publish_allowed(self):
        self.__client.publish(TOPIC_CURRENT_ALLOWED, payload=json.dumps(self.__allowed_ids))

    def __add_allowed(self, id):
        self.__allowed_ids.append(id)
        self.__save_to_file()
        self.__publish_allowed()
        print("allowed: " + str(self.__allowed_ids))

    def __try_remove_allowed(self, id):
        try:
            self.__allowed_ids.remove(id)
            self.__save_to_file()
        except ValueError:
            pass
        self.__publish_allowed()
        print("allowed: " + str(self.__allowed_ids))

    def __set_allowed(self, ids):
        self.__allowed_ids = ids
        self.__save_to_file()
        self.__publish_allowed()
        print("allowed: " + str(self.__allowed_ids))

    def __on_connect(self, client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        client.subscribe(TOPIC_ADD_ALLOWED)
        client.subscribe(TOPIC_REMOVE_ALLOWED)
        client.subscribe(TOPIC_SET_ALLOWED)
        client.subscribe(TOPIC_REQUEST_ALLOWED)

    def __on_message(self, client, userdata, msg):
        print("new message | " + msg.topic + " | " + str(msg.payload))

        if msg.topic == TOPIC_ADD_ALLOWED:
            request = json.loads(msg.payload)
            self.__add_allowed(request['id'])

        elif msg.topic == TOPIC_REMOVE_ALLOWED:
            request = json.loads(msg.payload)
            self.__try_remove_allowed(request['id'])

        elif msg.topic == TOPIC_SET_ALLOWED:
            request = json.loads(msg.payload)
            self.__set_allowed(request)

        elif msg.topic == TOPIC_REQUEST_ALLOWED:
            self.__publish_allowed()

    def __start(self):
        self.__client.connect(BROKER_HOST, BROKER_PORT)
        self.__client.loop_forever()

    def __start_async(self):
        thread = threading.Thread(target=self.__start, daemon=True)
        thread.start()

    def __init__(self):
        self.__client.on_connect = self.__on_connect
        self.__client.on_message = self.__on_message

        if path.exists(STORAGE):
            self.__load_from_file()
        else:
            self.__save_to_file()

        print("allowed: " + str(self.__allowed_ids))
        self.__start_async()


# repo = IdsRepository()
#
# while True:
#     t_id = input()
#     allowed = repo.check(t_id)
#     print(allowed)
#     if t_id == "exit":
#         break

'''

пример использования:
repo = IdsRepository()
is_qwerty_allowed = repo.check("qwerty")

check(id) возвращает True или False в зависимости от того, присутствует ли id в списке

список автоматически синхронизируется с файлом ./allowedIds.json
(файл будет создан, если не существует)

используется брокер broker.mqttdashboard.com
тут можно потыкать http://www.hivemq.com/demos/websocket-client/

описание топиков:

добавить в список
"testtopic/acs/77/add"
{"id":"qwerty"}

удалить из списка
testtopic/acs/77/remove
{"id":"qwerty"}

засетить новый список
testtopic/acs/77/set
["123","qwe"]

запросить список, который придет в testtopic/acs/77/current_allowed
testtopic/acs/77/request
# payload не важен

топик, в который приходит текущий список при любом изменении через add, remove или set
 либо при запросе testtopic/acs/77/request
testtopic/acs/77/current_allowed
["123", "qwe"]

топик, в который приходят все (успешные и не успешные) попытки входа
testtopic/acs/77/checks_log
{"id": "qwerty", "is_allowed": false}

'''

