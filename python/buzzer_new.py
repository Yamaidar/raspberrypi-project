from time import sleep
import RPi.GPIO as GPIO

class SimpleBuzzer:
    buzzer = None
    CL = [0, 131, 147, 165, 175, 196, 211, 248]  # Low C Note Frequency
    CM = [0, 262, 294, 330, 350, 393, 441, 495]  # Middle C Note Frequency
    CH = [0, 525, 589, 661, 700, 786, 882, 990]  # High C Note Frequency
    BuzzerPin = 11

    song_1 = [CH[1], CH[2], CH[3], CH[4], CH[5]]

    beat_1 = [1/4, 1/4, 1/4, 1/4, 1/4]

    song_2 = [CL[2]]

    beat_2 = [1]

    def play_success(self):
        print("play_success")
        self.play_tune(self.song_1, self.beat_1)

    def play_failure(self):
        print("play_failure")
        self.play_tune(self.song_2, self.beat_2)

    def play_tune(self, song, beat):
        self.buzzer.start(50)
        for i in range(0, len(song)):  # Play song 1
            self.buzzer.ChangeFrequency(song[i])  # Change the frequency along the s$
            sleep(beat[i] * 0.5)
        self.buzzer.stop()

    def __init__(self, pin):
        self.BuzzerPin = pin
        GPIO.setmode(GPIO.BOARD)  # Numbers GPIOs by physical location
        GPIO.setup(self.BuzzerPin, GPIO.OUT)  # Set pins' mode is output
        self.buzzer = GPIO.PWM(self.BuzzerPin, 440)  # 440 is initial frequency.

    def destroy(self):
        self.buzzer.stop()  # Stop the BuzzerPin
        GPIO.output(self.BuzzerPin, 1)  # Set BuzzerPin pin to High
        GPIO.cleanup()  #
