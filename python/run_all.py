from time import sleep

import RPi.GPIO as GPIO

from gpiozero import TonalBuzzer
from gpiozero.tones import Tone

from buzzer_new import SimpleBuzzer
from reader import IdsReader
from repo import IdsRepository

print("start all")

print("init reader...")
reader = IdsReader()
print("init repo...")
repo = IdsRepository()
print("init buzzer...")
buzzer = SimpleBuzzer(11)


try:
    while True:
        try:
            sleep(1)
            print("read")
            t_id = reader.read_id()
            allowed = repo.check(t_id)
            print(allowed)
            if allowed:
                print("sucess")
                buzzer.play_success()
            else:
                print("failure")
                buzzer.play_failure()
        finally:
            print("cycle finished")
except KeyboardInterrupt:
    print("shutting down...")
    buzzer.destroy()
